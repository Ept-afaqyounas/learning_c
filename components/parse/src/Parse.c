#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>

#include "http_get.h"
#include "httpserver.h"
#include "Parse.h"
#include "Cjsonoperation.h"

#define USER_CLASS_URL "http://parseapi.back4app.com/classes/test" // user id

parse_t parse_control;
static char *TAG = "PARSE INIT";

/*fun pointer pointing to fun with char * return type and void param*/
char *(*Http_getuid_ptr)(); 

/*callback function for user ID*/
void http_Server_uid_callback(char *(*funCallback)())
{
    Http_getuid_ptr = funCallback;
}


void PutPostURL()
{
    sprintf(parse_control.put_url, "%s%s%s", DEVICE_ID_URL, "/", parse_device_id());
    ESP_LOGI(TAG, "PARSE PUT URI :%s", parse_control.put_url);
}


void ParseInit()
{
    ESP_LOGD(TAG, "IN PARSE INIT");

    while (!HttpGetParse(USER_CLASS_URL))
    {
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }

    ExtractObjectId(parse_control.get_buff, Http_getuid_ptr()); // passed get_response_data and uid name fill device id
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    free(parse_control.get_buff);
}

void ParseStart()
{
    ParseInit();
}