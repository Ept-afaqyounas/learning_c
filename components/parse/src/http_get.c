#include <stdio.h>
#include <esp_log.h>
#include <string.h>
#include <stdlib.h>
#include <esp_http_client.h>
#include "cJSON.h"

#include "Parse.h"
#include "http_get.h"

static char *TAG = "HTTP_GET ";

bool HttpGetParse(char *URL){
    bool flag = 0;
    ESP_LOGI(TAG, "GET_URL:%s", URL);
    esp_err_t err = ESP_FAIL;

    esp_http_client_config_t config = {
        .url = URL,
    };

    esp_http_client_handle_t client = esp_http_client_init(&config);

    esp_http_client_set_header(client,PARSE_SERVER_APP_ID, PARSE_SERVER_APP_VAL);
    esp_http_client_set_header(client,PARSE_SERVER_REST_API_KEY, PARSE_SERVER_REST_API_VALUE);
    esp_http_client_set_header(client, CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);


    esp_http_client_set_method(client, HTTP_METHOD_GET);
    while (err != ESP_OK)
    {
        err = esp_http_client_open(client, 0);
        if (err != ESP_OK)
        {
            ESP_LOGE(TAG, "Failed to open HTTP-GET connection: %s", esp_err_to_name(err));
        }
        else
        {

            int HeadersData;

            HeadersData = esp_http_client_fetch_headers(client);

            int ContentLength = esp_http_client_get_content_length(client);
            printf("content length is : %d\n", ContentLength);
            parse_control.get_buff = (char *)malloc(ContentLength + 5);
            memset(parse_control.get_buff, 0, ContentLength + 5);
            if (HeadersData < 0)
            {
                ESP_LOGE(TAG, "HTTP GET client fetch headers failed");
                flag = 0;
            }
            else
            {
                int data_read = esp_http_client_read_response(client, parse_control.get_buff, ContentLength);
                if (data_read > 0)
                {
                    ESP_LOGD(TAG, "HTTP_DATA........%s", parse_control.get_buff);
                    printf("data------------------->%s", parse_control.get_buff);

                    flag = 1;
                }
                else
                {
                    ESP_LOGE(TAG, "Failed to read response");
                    flag = 0;
                }
            }
        }
    }

    ESP_LOGD(TAG, "OUT OF HTTPGETPARSE ........");
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return flag;
}
