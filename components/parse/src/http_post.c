#include <stdio.h>
#include <esp_log.h>
#include <string.h>
#include <stdlib.h>
#include <esp_http_client.h>
#include <cJSON.h>

#include "Parse.h"
#include "http_post.h"

static char *TAG = "HTTP_POST";

char *HttpPostParse(char *URL, char *buffer, int buffer_size)
{
    char *objectId = NULL;   //DID local variable 
    
    ESP_LOGI(TAG, "POST_URL:%s", URL);
    esp_err_t err = ESP_FAIL;

    esp_http_client_config_t config = {
        .url = URL,
    };

    esp_http_client_handle_t client = esp_http_client_init(&config);

    esp_http_client_set_header(client,PARSE_SERVER_APP_ID, PARSE_SERVER_APP_VAL);
    esp_http_client_set_header(client,PARSE_SERVER_REST_API_KEY, PARSE_SERVER_REST_API_VALUE);
    esp_http_client_set_header(client, CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);

    esp_http_client_set_method(client, HTTP_METHOD_POST);

    while (err != ESP_OK)
    {
        ESP_LOGI(TAG, "POST_BUFFER:%s", buffer);
        int HeadersData = 0;
        esp_http_client_set_header(client, "Content-Type", "application/json");

        err = esp_http_client_open(client, strlen(buffer));
        if (err != ESP_OK)
        {
            ESP_LOGE(TAG, "Failed to open HTTP connection: %s", esp_err_to_name(err));
            esp_http_client_cleanup(client);
        }
        else
        {
            int wlen = esp_http_client_write(client, buffer, strlen(buffer));
            if (wlen < 0)
            {
                ESP_LOGE(TAG, "Write failed");
            }

            HeadersData = esp_http_client_fetch_headers(client);
            if (HeadersData < 0)
            {
                ESP_LOGE(TAG, "HTTP client fetch headers failed");
            }
            else
            {
                int data_read = esp_http_client_read_response(client, buffer, buffer_size);
                if (data_read >= 0)
                {

                    if (esp_http_client_get_status_code(client) == 201)
                    {
                        // post is sucessful
                        cJSON *root = cJSON_Parse(buffer);
                        if (root)
                        {
                            cJSON *objectIdJson = cJSON_GetObjectItem(root, "objectId");
                            if (objectIdJson && objectIdJson->type == cJSON_String)
                            {
                                objectId = strdup(objectIdJson->valuestring);
                                printf("objectId: %s\n", objectId);
                            }
                            cJSON_Delete(root);
                        }
                    }
                    else if (esp_http_client_get_status_code(client) == 404)
                    {
                        esp_restart();  //TODO fix this
                    }
                }
                else
                {
                    ESP_LOGE(TAG, "Failed to read response");
                }
            }
        }
    }
    esp_http_client_close(client);
    esp_http_client_cleanup(client);
    return objectId;   //DID
}
