#include <string.h>
#include <esp_log.h>
#include <cJSON.h>

#include "Parse.h"
#include "Cjsonoperation.h"
#include "httpserver.h"
#include "http_post.h"

#define TAG "operations"
#define buffer_size 1024

char post_buffer[buffer_size];
char *object_id = NULL; // device id global
uint8_t onboarding_flag = 0;

/**
 * @brief Function that check object_id in parse in user class and compare it with
 *        with uid provided from save_credential post req. if UID matches means
 *        existing user.
 *
 * @param buff buffer containing content of get response
 * @param user_id user-id retrived from user in save_credential req
 * @return char* A character pointer of Device ID
 */
void ExtractObjectId(char *buff, char *user_id)
{
    if (strlen(buff) > 0)
    {
        int i;
        cJSON *user;
        char *ptr;
        cJSON *user_exist;

        ESP_LOGI(TAG, "Deserialize.....");

        cJSON *root = cJSON_Parse(buff);
        cJSON *array = cJSON_GetObjectItem(root, "results");
        int no_of_user = cJSON_GetArraySize(array);

        ESP_LOGI(TAG, "\nNo of devices->%d", no_of_user);
        for (i = 0; i < no_of_user; i++)
        {
            user = cJSON_GetArrayItem(array, i);
            user_exist = cJSON_GetObjectItem(user, "objectId");
            if (user_exist != NULL) // check if user ID column exists
            {
                ptr = cJSON_GetObjectItem(user, "objectId")->valuestring;
                if (strcmp(ptr, user_id) == 0) // check if there is existing user with same uid
                {
                    ESP_LOGD(TAG, "USER EXISTS");
                    break;
                }
                else
                {
                    ESP_LOGD(TAG, "USER DOES NOT EXISTS");
                }
            }
        }
        /* Post request to server creating a rack in device class*/
        snprintf(post_buffer, sizeof(post_buffer), "{}");
        object_id = HttpPostParse(DEVICE_ID_URL, post_buffer, buffer_size); // created an instance in device class return DID

        onboarding_flag = 1; // TODO fix
        /* Put URL for device id*/
        PutPostURL();
        printf("put url --->%s\n", parse_control.put_url);
        cJSON_Delete(root);
        // return object_id;
    }
    else
    {
        ESP_LOGI(TAG, "No data in get buffer.....");
        onboarding_flag = 0;
    }
}
char *parse_device_id()
{
    return object_id;  //return addr its name contain addr
}

uint8_t *onboarding_flag_fun()
{
    return &onboarding_flag; // return address of onboarding flag where it is stored
}