#ifndef HTTP_POST_H_
#define HTTP__POST_H_


#define PARSE_SERVER_APP_ID "X-Parse-Application-Id"
#define PARSE_SERVER_APP_VAL "7QF8Ffy1axRdeNFhsqz9c1jkrsCbblEPz355zqAv"
#define PARSE_SERVER_REST_API_KEY "X-Parse-REST-API-Key"
#define PARSE_SERVER_REST_API_VALUE "BRYSzwYXnGjsWoGQRL64xUZA8aoK0mpyE1Qan2se"
#define CONTENT_TYPE_KEY "Content-Type"
#define CONTENT_TYPE_VALUE "application/json"

/**
 * @brief 
 * 
 * @param URL 
 * @param buffer 
 * @param buffer_size 
 * @return char* device id as pointer return
 */
char *HttpPostParse(char *URL, char *buffer, int buffer_size);

#endif // HTTP_H_