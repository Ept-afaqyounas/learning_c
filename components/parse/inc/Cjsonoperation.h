#ifndef _CJSONOPERATION_H_
#define _CJSONOPERATION_H_

#define DEVICE_ID_URL "http://parseapi.back4app.com/classes/B4aVehicle"
/**
 * @brief Extracts Object ID of given instance
 *
 * @param buff
 * @param uid
 * @return char*
 */
void ExtractObjectId(char *buff, char *user_id);

char *parse_device_id();

uint8_t *onboarding_flag_fun();

#endif