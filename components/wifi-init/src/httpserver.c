#include <string.h>
#include <esp_log.h>
#include <sys/param.h>
#include <esp_netif.h>
#include <esp_wifi.h>
#include <cJSON.h>

#include "eeprom_rw.h"
#include "httpserver.h"
#include "wifi_scan.h"
#include "chip_mac.h"
#include "Cjsonoperation.h"

#include "wifi_apsta.h"
#include "wifi_common.h"
#include "wifi_st.h"
#include "wifi_ap.h"

#define EEPROM_NAMESPACE CONFIG_ESP_EEPROM_NAMESPACE

static const char *TAG = "HTTP SERVER";
static const char *TAGs = "JSON";

httpd_handle_t server = NULL;
httpd_req_t *req = NULL;

char *uid = NULL;

/**
 * @brief Pointer Function which is linking generic device id
 * 
 */
char *(*device_ID)();

/**
 * @brief Pointer Function which is linking any generic Host like parse you want to start
 *
 */
void (*GenericHost)();

/**
 * @brief Parse a received JSON packet and extract SSID and password values.
 *
 * This function parses a JSON packet and extracts the SSID and password values from it.
 * These values are stored in char pointers, which are then used to write data to EEPROM.
 *
 * @param buf The JSON packet to parse.
 * @param ssid Pointer to a char pointer where the SSID will be stored.
 * @param password Pointer to a char pointer where the password will be stored.
 * @return esp_err_t indicating the success or failure of JSON parsing and value extraction.
 *   - ESP_OK if the JSON packet was successfully parsed and valid values were extracted.
 *   - ESP_ERR_INVALID_ARG if either the SSID or password is empty.
 *   - ESP_FAIL if the input data is not in JSON format.
 */
static esp_err_t jsonToSsid(char *buf, char **ssid, char **password, char **user_id)
{
    cJSON *root = cJSON_Parse(buf);

    if (root)
    {
        *ssid = cJSON_GetObjectItem(root, "ssid")->valuestring;
        *password = cJSON_GetObjectItem(root, "password")->valuestring;
        *user_id = cJSON_GetObjectItem(root, "UID")->valuestring;

        if (strlen(*ssid) == 0 || strlen(*password) == 0 || strlen(*user_id) == 0)
        {
            // SSID and pass empty
            cJSON_Delete(root);
            return ESP_ERR_INVALID_ARG;
        }

        ESP_LOGI(TAGs, "SSID: %s", *ssid);
        ESP_LOGI(TAGs, "Password: %s", *password);
        ESP_LOGI(TAGs, "User ID: %s", *user_id);
        return ESP_OK;
    }
    else // Not a json format
    {
        cJSON_Delete(root);
        return ESP_FAIL;
    }
    return ESP_FAIL;
}
/**
 * @brief Handle an HTTP request to parse a received JSON packet, retrieve stored values, and
 * write them to EEPROM.
 *
 * This function processes an HTTP request by parsing the received JSON packet, extracting
 * the stored values, and filling a `key_values` structure that is passed to the `eeprom_write`
 * function to write the values into EEPROM.
 *
 * @param req HTTP request object representing the client's request.
 * @return esp_err_t indicating the success or failure of the request handling.
 *   - ESP_OK if the request was handled successfully.
 *   - ESP_FAIL if an error occurred during request processing.
 */
static esp_err_t save_handler(httpd_req_t *req)
{
    char buf[1000];
    int ret, remaining = req->content_len; // initialized remaining with content lenght of http req

    if (remaining == 0)
    {
        // Handle completely empty request body
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Empty request body");
        return ESP_FAIL;
    }

    while (remaining > 0)
    {
        /* Read the data for the request */
        ret = httpd_req_recv(req, buf, MIN(remaining, sizeof(buf)));
        if (ret <= 0)
        {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT)
            {
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }
        remaining -= ret;

        /*char pointers to store reterived ssid and pass and uid from parsed json buffer*/
        char *ssid_val = NULL, *password_val = NULL;
        esp_err_t status = jsonToSsid(buf, &ssid_val, &password_val, &uid);

        if (status == ESP_OK)
        {
            eeprom_key_value_t key_values[2] = {{.key = "wifi_ssid", .data = ssid_val, .data_type = STRING}, {.key = "wifi_password", .data = password_val, .data_type = STRING}};

            /*Switch to APSTA mode*/
            wifi_mode_t mode;
            esp_wifi_get_mode(&mode);
            if (mode == WIFI_MODE_APSTA) // mode apsta set by wifiscan
            {
                config_wifi_sta(ssid_val, password_val);
                wifi_start();
            }

            else if (mode == WIFI_MODE_AP)
            {
                Wifi_mode_apsta();
                config_wifi_sta(ssid_val, password_val);
                wifi_start();
            }

            vTaskDelay(50000 / portTICK_PERIOD_MS);

            /*Saving Wifi Credential in NVS*/
            esp_err_t err = eeprom_write(EEPROM_NAMESPACE, key_values, 2);

            if (err != ESP_OK)
            {
                ESP_LOGE(TAG, "Error writing to NVS: %s", esp_err_to_name(err));
                httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Memory allocation failed");
            }

            /*Starting Parse for backend Communication Call backfunction*/
            // if (callbackLinkted)
            GenericHost();
            ESP_LOGI(TAG, "Device id :>%s\n\n", device_ID());

            /*Saving DID in NVS*/
            eeprom_key_value_t device_id_key_value[1] = {{.key = "device_id", .data = device_ID(), .data_type = STRING}};
            eeprom_write(EEPROM_NAMESPACE, device_id_key_value, 1);

            /*Sending DID in Response*/
            char deviceid[100];
            sprintf(deviceid, "Device ID : %s", device_ID());
            httpd_resp_send(req, device_ID(), HTTPD_RESP_USE_STRLEN);

            /*Writing onboarding flag in nvs*/
            eeprom_key_value_t onboarding_key_value[1] = {{.key = "onboarding_flag", .data = onboarding_flag_fun(), .data_type = UINT8}};
            eeprom_write(EEPROM_NAMESPACE, onboarding_key_value, 1);

            ESP_LOGI(TAG, "Device id :>%s\n\n", device_ID());
            esp_restart();

        }
        else if (status == ESP_ERR_INVALID_ARG)
        {
            httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, " Invalid Empty ssid or Password Field");
            return ESP_FAIL;
        }
        else // not json format this this error
        {
            httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Invalid Json body");
            return ESP_FAIL;
        }
    }

    return ESP_OK;
}
/**
 * @brief Handle an HTTP request to scan nearby Wi-Fi networks and respond with a JSON packet.
 *
 * This function is responsible for processing an HTTP request and initiating a Wi-Fi scan
 * of nearby networks. It then creates a JSON packet containing the scan results and sends
 * it as the HTTP response.
 *
 * @param req HTTP request object representing the client's request.
 * @return esp_err_t indicating the success or failure of the request handling.
 *   - ESP_OK if the request was handled successfully.
 *   - An error code if there was an issue handling the request.
 */
static esp_err_t ssid_handler(httpd_req_t *req)
{
    char *json_string = NULL;
    wifi_mode_t mode;
    esp_wifi_get_mode(&mode);
    if (mode == WIFI_MODE_STA || mode == WIFI_MODE_APSTA)
    {
        json_string = wifiscan();
    }
    else if (mode == WIFI_MODE_AP)
    {

        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
        json_string = wifiscan();
    }

    httpd_resp_send(req, json_string, HTTPD_RESP_USE_STRLEN);
    free(json_string);
    return ESP_OK;
}

static const httpd_uri_t save_credentials = {.uri = "/savecredentials", .method = HTTP_POST, .handler = save_handler, .user_ctx = NULL};
static const httpd_uri_t simot = {.uri = "/ssid_list", .method = HTTP_GET, .handler = ssid_handler, .user_ctx = NULL};

esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err)
{
    httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "API not found");
    return ESP_FAIL;
}

httpd_handle_t start_webserver(void)
{

    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.lru_purge_enable = true;

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK)
    {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &simot);
        httpd_register_uri_handler(server, &save_credentials);

#if CONFIG_EXAMPLE_BASIC_AUTH
        httpd_register_basic_auth(server);
#endif
        return server;
    }

    ESP_LOGI(TAG, "Error starting server!");
    return NULL;
}

void stop_webserver(httpd_handle_t server)
{
    // Stop the httpd server
    httpd_stop(server);
}

char *Http_server_getuid(void)
{
    return uid;
}


void http_Server_link_host_callback(void (*hostCallback)())
{
    GenericHost = (void *)hostCallback;
}

/*callback function for device id*/
void http_Server_DID_callback(char *(*DID_Callback)())
{
    device_ID = DID_Callback;
}