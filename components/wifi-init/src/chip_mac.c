#include "esp_system.h"
#include "esp_mac.h"

/**
 * @brief Get the mac addr object
 * 
 * Function that get the mac address of the chip and fill the buffer mac[].
 * 
 * @param mac_str character pointer where human readble string is stored 
 */
void get_mac_addr(char *mac_str)
{
    uint8_t mac[6];
    esp_efuse_mac_get_default(mac);

    sprintf(mac_str, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}