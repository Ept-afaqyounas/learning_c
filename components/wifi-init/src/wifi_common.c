#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <esp_wifi.h>
#include <esp_event.h>

esp_netif_t *esp_netif_sta = NULL;
esp_netif_t *esp_netif_ap  = NULL;

esp_event_handler_instance_t instance_any_id;
esp_event_handler_instance_t instance_got_ip;

void event_register(void (*e_handler)(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data))
{
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, e_handler, NULL, &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP, e_handler, NULL, &instance_got_ip));
}

void wifi_app_default_init()
{

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    esp_netif_create_default_wifi_sta();
    esp_netif_create_default_wifi_ap();
}

void Wifi_mode_ap()
{
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
}

void Wifi_mode_sta()
{
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
}

void Wifi_mode_apsta()
{
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
}

/**
 * @brief Initialize Wi-Fi in Access Point (AP) mode with custom configuration.
 *
 * This function initializes the Wi-Fi module in Access Point (AP) mode with the provided configuration.
 * It sets up the AP with the specified SSID, channel, maximum allowed connections, and authentication mode.
 *
 * @param e_handler Event handler function to handle Wi-Fi events.
 *                  Pass a function pointer to your custom event handler that can respond to Wi-Fi events.
 */

void wifi_start()
{
    ESP_ERROR_CHECK(esp_wifi_start());
}