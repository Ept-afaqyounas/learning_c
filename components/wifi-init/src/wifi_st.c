#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>

#include "wifi_st.h"
#include "wifi_apsta.h"
#include "httpserver.h"
#include "wifi_common.h"

/**
 * @brief Initialize the Wi-Fi station mode and connect to a specified network.
 *
 * This function initializes the ESP32 Wi-Fi station mode, configures it to connect
 * to a specified network with the provided SSID and password, and sets up event handlers
 * to monitor Wi-Fi events.
 *
 * @param wifiSSID The SSID of the network to which the station should connect.
 * @param wifiPASS The password of the network.
 */
void config_wifi_sta(const void *wifiSSID, const void *wifiPASS)
{
    wifi_config_t wifi_config = {
                                            .sta =
                                                                                    {
                                                                                                                            .threshold.authmode = WIFI_AUTH_WPA_WPA2_PSK,
                                                                                                                            .sae_pwe_h2e        = WPA3_SAE_PWE_BOTH,
                                                                                    },
    };

    sprintf((char *)wifi_config.sta.ssid, wifiSSID);
    sprintf((char *)wifi_config.sta.password, wifiPASS);
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
}
