#include <string.h>
#include <esp_event.h>
#include <nvs_flash.h>

#include "eeprom_rw.h"
#include "wifi_ap.h"
#include "wifi_st.h"
#include "event_handler.h"
#include "wifi_init.h"
#include "wifi_common.h"
#include "wifi_apsta.h"

#include "Cjsonoperation.h"
#include "Parse.h"

char wifiSSID[CONFIG_ESP_WIFI_SSID_SIZE];
char wifiPASS[CONFIG_ESP_WIFI_PASS_SIZE];
char dev[100];

TaskHandle_t wifiInitHandle = NULL;

/**
 * @brief Initializes Wi-Fi connection based on stored credentials or enters Access Point (AP) mode.
 *
 * This function is responsible for initializing the Wi-Fi connection. It checks if the credentials
 * (SSID and password) are present in EEPROM using the eeprom_read function. If the credentials are found,
 * it initializes Wi-Fi in Station (STA) mode and attempts to connect to the network. If the credentials
 * are not found, it initializes Wi-Fi in Soft Access Point (AP) mode.
 *
 * @param pvParameters A void pointer used to pass information/data of key/value pairs to be read from EEPROM.
 */
void initWifiConTask(void *pvParameters)
{
    eeprom_key_value_t read_key_values[] = {
        {.key = "wifi_ssid", .data = wifiSSID, .data_type = STRING, .size = sizeof(wifiSSID)},
        {.key = "wifi_password", .data = wifiPASS, .data_type = STRING, .size = sizeof(wifiPASS)},
        {.key = "onboarding_flag", .data = onboarding_flag_fun(), .data_type = UINT8}
    };
    esp_err_t is_ssid_available = eeprom_read(CONFIG_ESP_EEPROM_NAMESPACE, read_key_values, 2);

    wifi_app_default_init();
    event_register(event_handler);

    if (is_ssid_available == ESP_OK && *onboarding_flag_fun()==1)  //TODO fix
    {
        eeprom_key_value_t aaa[] = {
            {.key = "device_id", .data = parse_device_id(), .data_type = STRING, .size = sizeof(dev)}};
        esp_err_t available = eeprom_read(CONFIG_ESP_EEPROM_NAMESPACE, aaa, 1);
        if (available == ESP_OK)
        {
            printf("DEVICE ID IS SAVED IN NVS-->\n\n\n");
        }

        /*switch mode to STA and config STA*/
        Wifi_mode_sta();
        config_wifi_sta(wifiSSID, wifiPASS);
    }
    else
    {
        config_AP();
        Wifi_mode_ap();
    }

    wifi_start();

    while (1)
    {
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void wifiInit(void)
{
    xTaskCreate(&initWifiConTask, "Wifi Init", 4096 * 4, NULL, 1, &wifiInitHandle);
}