#include <string.h>
#include <esp_wifi.h>
#include <esp_log.h>
#include <cJSON.h>

#include "wifi_scan.h"

#define NUMBER_OF_SSID_LIST 10
static const char *TAGs = "Wifi Scan";

wifi_ap_record_t ap_record[NUMBER_OF_SSID_LIST];  // to hold the found AP
uint16_t ap_count = NUMBER_OF_SSID_LIST;          // max ap number ap_record can store

/**
 * @brief Create a JSON packet of scanned Wi-Fi SSIDs.
 *
 * This function creates a JSON packet containing the list of scanned Wi-Fi SSIDs.
 *
 * @return char* A pointer to the JSON string representing the scanned SSIDs.
 */
static char *ssidToJson()
{
    cJSON *ssid_array = NULL;
    cJSON *ssid       = NULL;
    cJSON *root       = NULL;
    char *json_string = NULL;

    // Create a cJSON object to hold the SSID list
    root       = cJSON_CreateObject();
    ssid_array = cJSON_CreateArray();
    for (int i = 0; i < ap_count; i++)
    {
        ssid = cJSON_CreateString((const char *)ap_record[i].ssid);
        cJSON_AddItemToArray(ssid_array, ssid);  // add(x,y)
    }
    cJSON_AddItemToObject(root, "SSID", ssid_array);  // ssid_array -> object root

    json_string = cJSON_Print(root);
    ESP_LOGI(TAGs, "%s", json_string);

    cJSON_Delete(root);
    return json_string;
}

/**
 * @brief Perform a Wi-Fi scan and store the results in ap_record structure.
 *
 * This function initiates a Wi-Fi scan and stores the results in the ap_record structure.
 *
 * @return char* A pointer to the JSON string representing the scanned SSIDs.
 */
char *wifiscan()
{
    char *json_string = NULL;

    wifi_scan_config_t scan_config = {.ssid        = NULL,
                                      .bssid       = NULL,
                                      .channel     = 0,
                                      .show_hidden = false,
                                      .scan_time   = {

                                                                              .active =
                                                                                                                      {

                                                                                                                                                              .min = 10,  // Minimum active scan time
                                                                                                                                                              .max = 20,  // Maximum active scan time
                                                                                                                      },
                                                                              .passive = 0,

                                      }};
    ESP_ERROR_CHECK(esp_wifi_scan_start(&scan_config, true));
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&ap_count, ap_record));

    json_string = ssidToJson();
    return json_string;
}