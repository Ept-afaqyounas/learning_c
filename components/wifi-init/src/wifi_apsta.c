#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <esp_wifi.h>
#include <esp_event.h>

#include "wifi_st.h"
#include "wifi_ap.h"
#include "wifi_common.h"
#include "wifi_init.h"
#include "wifi_apsta.h"

void config_apsta()
{
    config_AP();
    config_wifi_sta(wifiSSID, wifiPASS);
}