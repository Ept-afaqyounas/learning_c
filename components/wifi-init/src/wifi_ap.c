#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_wifi.h>
#include <esp_log.h>
#include <esp_event.h>

#include "wifi_ap.h"
#include "wifi_st.h"
#include "wifi_apsta.h"
#include "http_server.h"
#include "wifi_common.h"

#define MY_ROUTER_SSSID        CONFIG_ESP_WIFI_SSID
#define MY_ROUTER_PASSCODE     CONFIG_ESP_WIFI_PASSWORD
#define MY_ROUTER_WIFI_CHANNEL CONFIG_ESP_WIFI_CHANNEL
#define MY_ROUTER_MAX_STA_CONN CONFIG_ESP_MAX_STA_CONN
#define DEFAULT_SCAN_LIST_SIZE CONFIG_EXAMPLE_SCAN_LIST_SIZE

const char *TAG = "wifi softAP";

void config_AP()
{
    wifi_config_t wifi_config = {

                                            .ap = {.ssid = MY_ROUTER_SSSID, .ssid_len = strlen(MY_ROUTER_SSSID), .channel = MY_ROUTER_WIFI_CHANNEL, .max_connection = MY_ROUTER_MAX_STA_CONN, .password = MY_ROUTER_PASSCODE, .authmode = WIFI_AUTH_WPA2_PSK

                                            },
    };

    if (strlen(MY_ROUTER_PASSCODE) == 0)
    {
        wifi_config.ap.authmode = WIFI_AUTH_OPEN;
    }

    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));
}
