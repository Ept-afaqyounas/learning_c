#include <esp_event.h>
#include <esp_err.h>
#include <esp_http_server.h>
#include <esp_wifi.h>
#include <esp_log.h>
#include <esp_netif_types.h>

#include "httpserver.h"
#include "esp_mac.h"
#include "wifi_common.h"
#include "wifi_apsta.h"

extern httpd_handle_t server;

static uint8_t sta_retry;
static const char *TAGS = "event_handler";

/**
 * @brief Event handler function which handles WIFI and IP events of AP and STA mode.
 *
 * This function is responsible for handling WIFI and IP events in both AP and STA mode.
 * Depending on the event, it starts or stops a web server.
 *
 * @param arg Pointer to user-defined data (can be NULL).
 * @param event_base The event base associated with the event.
 * @param event_id The event ID.
 * @param event_data Pointer to event-specific data.
 */
void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    if (event_base == WIFI_EVENT)
    {
        wifi_event_ap_staconnected_t *event_stacon;
        wifi_event_ap_stadisconnected_t *event_stadis;

        switch (event_id)
        {
        // Event arises when esp_wifi_start return ESP_OK in STA mode or apsta.
        case WIFI_EVENT_STA_START:
            ESP_LOGI(TAGS, "Wifi Station Started");
            esp_wifi_connect();
            break;

        // Event arises when esp_wifi_connect failed to connect or esp_disconnect is called etc.
        case WIFI_EVENT_STA_DISCONNECTED:
            if (sta_retry < CONFIG_ESP_WIFI_DIS_RETRY) // 5 retries
            {
                ESP_LOGI(TAGS, "Wifi lost connection, retrying");
                esp_wifi_connect();
                sta_retry++;
            }
            else
            {
                ESP_LOGE(TAGS, "Connection to the Access Point failed");
                ESP_LOGI(TAGS, "Switch to AP Mode for new Connection");

                Wifi_mode_apsta();
                config_apsta();
                wifi_start();
            }
            break;

        case WIFI_EVENT_AP_STACONNECTED:

            event_stacon = (wifi_event_ap_staconnected_t *)event_data;
            ESP_LOGI(TAGS, "station " MACSTR " join, AID=%d", MAC2STR(event_stacon->mac), event_stacon->aid);
            start_webserver();
            break;

        case WIFI_EVENT_AP_STADISCONNECTED:

            event_stadis = (wifi_event_ap_stadisconnected_t *)event_data;
            ESP_LOGI(TAGS, "station " MACSTR " leave, AID=%d", MAC2STR(event_stadis->mac), event_stadis->aid);
            stop_webserver(server);
            break;

        default:
            break;
        }
    }
    else if (event_base == IP_EVENT)
    {
        ip_event_got_ip_t *event;

        switch (event_id)
        {
        // Event arises when ESP is conenct to access point and assigned IP
        case IP_EVENT_STA_GOT_IP:
            event = (ip_event_got_ip_t *)event_data;
            ESP_LOGI(TAGS, "IP Address:" IPSTR, IP2STR(&event->ip_info.ip));
            sta_retry = 0;
            break;
        }
    }
}
