idf_build_get_property(target IDF_TARGET)

idf_component_register(SRCS "src/event_handler.c"
                          "src/httpserver.c"
                          "src/wifi_ap.c"
                          "src/wifi_st.c"
                          "src/wifi_init.c"
                          "src/wifi_scan.c"
                          "src/wifi_common.c"
                          "src/wifi_apsta.c"
                          "src/chip_mac.c"
                     INCLUDE_DIRS "inc"
                     REQUIRES nvs_flash esp_http_server json esp_wifi parse eeprom  # Add these dependencies
                     )