#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include <stdint.h>
#include <esp_event.h>

/**
 * @brief Event handler function which handles WIFI and IP events of AP and STA mode.
 *
 * This function is responsible for handling WIFI and IP events in both AP and STA mode.
 * Depending on the event, it starts or stops a web server.
 *
 * @param arg Pointer to user-defined data (can be NULL).
 * @param event_base The event base associated with the event.
 * @param event_id The event ID.
 * @param event_data Pointer to event-specific data.
 */
void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);

#endif  //EVENT_HANDLER_H
