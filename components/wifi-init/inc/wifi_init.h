#ifndef WIFI_INIT_H__
#define WIFI_INIT_H__


extern char wifiSSID[CONFIG_ESP_WIFI_SSID_SIZE];
extern char wifiPASS[CONFIG_ESP_WIFI_PASS_SIZE];

/**
 * @brief Wifi initializtion function that creates a FreeRTOS task, that checks if there are desired key/value
 * are present in NVS if present then i initializes STA mode and tries to connect to it otherwise it configure 
 * itself in AP mode read and write in EEPROM.
 * 
 */
void wifiInit(void);


#endif  //WIFI_INIT_H__