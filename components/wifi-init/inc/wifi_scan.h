#ifndef WIFI_SCAN_H__
#define WIFI_SCAN_H__

/**
 * @brief Wifiscan function that scan nearby wifi networks and create a json packet.
 * 
 * @return char* 
 */
char *wifiscan();

#endif  //WIFI_SCAN_H__