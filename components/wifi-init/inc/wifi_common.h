
#ifndef WIFI_COMMON_H
#define WIFI_COMMON_H

#include <esp_event.h>
#include <stdint.h>

void wifi_app_default_init();

void event_register(void (*e_handler)(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data));

void Wifi_mode_ap();
void Wifi_mode_sta();
void Wifi_mode_apsta();

void wifi_start();

#endif  // WIFI_COMMON_H