WiFi Initialization Component Documentation
===========================================

The WiFi Initialization component in this project provides functionality for initializing and managing WiFi connectivity on an ESP microcontroller. This documentation explains the implementation details of the WiFi Initialization component.

Component Overview
------------------

The WiFi Initialization component consists of functions and configurations for setting up and managing WiFi connectivity on an ESP microcontroller.

Prerequisites
-------------

Before using this component, ensure you have the following prerequisites:

- **ESP-IDF**: This component is designed for use with the Espressif IoT Development Framework (ESP-IDF). Make sure you have it installed and configured.

Installation
-------------

To include the WiFi Initialization component in your ESP-IDF project, follow these steps:

1. Add it as a submodule to your project, in your root project directory or in your components directory.

2. Configure the component in your project's CMakeLists.txt.

    ```cmake
    set(EXTRA_COMPONENT_DIRS ${CMAKE_SOURCE_DIR}/components/wifi_init)
    ```

3. Build your project using `idf.py build`.

Configuration
-------------

The WiFi Initialization component can be configured to meet your project's specific requirements. Configuration options can be found in the `kconfig` file, where you can set parameters such as WiFi SSID, password, and more.
You can also set the wifi configuration from idf.menuconfig as shown in figure below:



#. Run idf.py menuconfig
   
#. Search and goto Component configuration.

        .. image:: ../../doc/component_config.png
 
#. In Component Configuration search for **Wifi Init config** and inside it goto **Wi-fi Config**
           
        .. image:: ../../doc/init.png

#. You can set your esp AP mode Configuration like its SSID and Password. Maximum number
   of station in STA or APSTA mode that can connect to it, ssid and password size and retries. 
   
            .. image:: ../../doc/cred.png

Usage
-----

To use the WiFi Initialization component in your project, follow these steps:

1. Include the component in your project as described in the Installation section.

2. Configure the WiFi Initialization component by setting appropriate parameters.

3. To use the functionality of the component like initializing wifi to check Wifi_credential in NVS you have to include libraries of function like:
    
    ```
    #include "wifi_init.h"
    ```
As this module also read and write from NVS so we 
.. API Reference
.. --------------

.. The WiFi Initialization component provides several functions for working with WiFi connectivity. For detailed information about these functions, please refer to the [API documentation](link-to-api-docs-here).

.. Examples
.. --------

.. To help you get started, we've included example projects that demonstrate how to use the WiFi Initialization component in various scenarios.

Contributing
------------

We welcome contributions from the community to enhance and improve the WiFi Initialization component. If you'd like to contribute, please follow these steps:

1. Report any issues or bugs on the project's GitHub Issues page.

Copyright (c) 2023
