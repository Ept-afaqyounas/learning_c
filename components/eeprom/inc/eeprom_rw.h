#ifndef EEPROM_RW_H_
#define EEPROM_RW_H_

#include <stdint.h>

/**
 * @brief Enumeration for data types used in EEPROM storage.
 */
typedef enum
{
    STRING,
    UINT8,
    UINT16,
    UINT32,
} eeprom_datatype_t;

/**
 * @brief Structure representing key-value pairs for EEPROM storage.
 */
typedef struct
{
    const char *key;
    const void *data;
    size_t size;
    eeprom_datatype_t data_type;

} eeprom_key_value_t;

esp_err_t eeprom_read(const char *namespace_name, const eeprom_key_value_t *key_values, size_t num_key_values);
esp_err_t eeprom_write(const char *namespace_name, const eeprom_key_value_t *key_values, size_t num_key_values);

#endif
