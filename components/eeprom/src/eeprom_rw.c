#include <string.h>
#include <nvs_flash.h>
#include <esp_log.h>
#include <esp_system.h>

#include "eeprom_rw.h"

static const char *TAG = "NVS :";

/**
 * @brief Write data to the Non-Volatile Storage (NVS).
 *
 * This function writes data to the NVS in the specified namespace. It supports writing
 * different data types such as strings, uint8_t, uint16_t, and uint32_t. The data is provided
 * in an array of `eeprom_key_value_t` structures.
 *
 * @param namespace_name The NVS namespace where the data will be stored.
 * @param key_values A pointer to an array of `eeprom_key_value_t` structures containing the data
 * to be written.
 * @param num_key_values The number of key-value pairs to write to NVS.
 * @return esp_err_t An `esp_err_t` value indicating the success or failure of the write operation.
 *   - ESP_OK: Write operation completed successfully.
 *   - Other error codes if there was an issue with the write operation.
 */
esp_err_t eeprom_write(const char *namespace_name, const eeprom_key_value_t *key_values, size_t num_key_values)
{

    nvs_handle_t nvs_handle;
    esp_err_t err;
    err = nvs_flash_init();
    if (err != ESP_OK)
    {
        ESP_LOGI(TAG, "Error initializing nvs");
        return err;
    }

    err = nvs_open(namespace_name, NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK)
    {
        ESP_LOGI(TAG, "Error opening NVS namespace\n");
        return err;
    }

    for (uint8_t i = 0; i < num_key_values; i++)
    {
        switch (key_values[i].data_type)
        {
            case STRING: err = nvs_set_str(nvs_handle, key_values[i].key, (const char *)key_values[i].data); break;
            case UINT8: err = nvs_set_u8(nvs_handle, key_values[i].key, *((uint8_t *)key_values[i].data)); break;
            case UINT16: err = nvs_set_u16(nvs_handle, key_values[i].key, *((uint16_t *)key_values[i].data)); break;
            case UINT32: err = nvs_set_u32(nvs_handle, key_values[i].key, *((uint32_t *)key_values[i].data)); break;
            default: err = ESP_FAIL; break;
        }
        if (err != ESP_OK)
        {
            ESP_LOGE(TAG, "Error writing to NVS key %s: %s", key_values[i].key, esp_err_to_name(err));
            nvs_close(nvs_handle);
            return err;
        }
    }

    nvs_commit(nvs_handle);
    nvs_close(nvs_handle);
    return ESP_OK;
}

/**
 * @brief Read data from the Non-Volatile Storage (NVS).
 *
 * This function reads data from the NVS in the specified namespace. It supports reading
 * different data types such as strings, uint8_t, uint16_t, and uint32_t. The data is retrieved
 * based on the keys provided in an array of `eeprom_key_value_t` structures.
 *
 * @param namespace_name The NVS namespace from which to read data.
 * @param key_values A pointer to an array of `eeprom_key_value_t` structures containing the keys
 * from which to read and store the data.
 * @param num_key_values The number of key-value pairs to read from NVS.
 * @return esp_err_t An `esp_err_t` value indicating the success or failure of the read operation.
 *   - ESP_OK: Read operation completed successfully.
 *   - Other error codes if there was an issue with the read operation.
 */
esp_err_t eeprom_read(const char *namespace_name, const eeprom_key_value_t *key_values, size_t num_key_values)
{
    nvs_handle_t nvs_handle;
    esp_err_t err;

    // Initialize NVS
    err = nvs_flash_init();
    if (err != ESP_OK)
    {
        ESP_LOGI(TAG, "Error initializing NVS\n");
        return err;
    }
    // Open NVS namespace
    err = nvs_open(namespace_name, NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK)
    {
        ESP_LOGI(TAG, "Error opening NVS namespace\n");
        return err;
    }

    // Read SSID NVS
    for (uint8_t i = 0; i < num_key_values; i++)
    {
        size_t data_size = key_values[i].size;

        switch (key_values[i].data_type)
        {
            case STRING: err = nvs_get_str(nvs_handle, key_values[i].key, (char *)key_values[i].data, &data_size); break;
            case UINT8: err = nvs_get_u8(nvs_handle, key_values[i].key, (uint8_t *)key_values[i].data); break;
            case UINT16: err = nvs_get_u16(nvs_handle, key_values[i].key, (uint16_t *)key_values[i].data); break;
            case UINT32: err = nvs_get_u32(nvs_handle, key_values[i].key, (uint32_t *)key_values[i].data); break;
            default: err = ESP_FAIL; break;
        }
    }  // after loop end we check if read was sucessfull ESP_OK.

    if (err != ESP_OK)
    {
        nvs_close(nvs_handle);
        return err;
    }

    nvs_close(nvs_handle);
    return ESP_OK;
}
