## Branching Rules

If a particular task or issue have created then;
    - There will be an issue-no like issue-12
    - There will be task name like backend user data communication

So, the branch should be created accordingly. If it is a bug/feature branch then the branch name should be like;

    - <feature/bugfix>/<issue-id>-<task-done>-<issue-name>
For example;

    - feature/i12-backend-user-data-communication