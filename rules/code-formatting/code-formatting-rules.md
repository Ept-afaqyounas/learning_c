## Clang-Format Rules Checklist

List of items that must be checked before applying clang format;

* [ ] `clang-format` utility must be installed on the HOST system
* [ ] Project's parent directory must contain the `.clang-format` file

Note: Please install clang-format version: 14
