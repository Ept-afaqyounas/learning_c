## Commit Message Rules Checklist

List of items that must be a part of commit message;

* [ ] Commit message must have enough information to help viewers understand the feature/bug and its functional description.
* [ ] Commit message must be atleat `50 char` long.
* [ ] The commit title should describe the changes, i.e. "Ethernet: Added support for Ethernet" or "CMake: fix some issues for CMake".
* [ ] Add commit branch name at the end of commit message in square brackets, `[<branch-name>]`.
* [ ] Check if commit message is descriptive, precise and clean.