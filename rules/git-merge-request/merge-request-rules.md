## Merge Request Rules Checklist

List of things that must be fulfilled for a merge request;

* [ ] Check if merge request description has enough info to help reviewers understand the issue, its root cause, impact, and the proposed solution.
* [ ] Check if merge request description has enough info to help reviewers understand the feature, its functional description, example, documentation, test cases, test results, feature TODO list.
* [ ] The MR title describes the change, including the component name, ie "lwip: Add support for IP over Pigeon".
* [ ] All related links — links to gitlab issues etc, should be mentioned in the `Related` subsection.
* [ ] Add label for the area this MR is part of. Add check if other necessary labels are added.
* [ ] For documentation updates, check if the `Docs` is up to date.
* [ ] Check if this is a breaking change. If it is, add notes to the `Breaking change notes` subsection.
* [ ] `Release` note entry if this is a new public feature, or a fix for an issue introduced in the previous release.
* [ ] Check if commit log is clean and precise.
* [ ] All relevant CI jobs have been run, i.e. jobs which cover the code changed by the MR.

**Note:** The above checklist can also be a part of merge request description.