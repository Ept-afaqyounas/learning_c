#include <string.h>
#include <esp_event.h>
#include <esp_log.h>
#include <nvs_flash.h>

#include "httpserver.h"
#include "wifi_init.h"
#include "version.h"
#include "Parse.h"
#include "Cjsonoperation.h"

void app_main()
{

    ESP_LOGI(TAG, "%d.%d.%d", VER_MAJOR, VER_MINOR, VER_PATCH);

    // Initial Config
    wifiInit();
    // as an example
    http_Server_link_host_callback(&ParseStart);

    http_Server_uid_callback(&Http_server_getuid);

    http_Server_DID_callback(&parse_device_id);
    // Apps init
}
