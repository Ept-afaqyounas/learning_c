#!/bin/bash

FULL_PATH_TO_SCRIPT="$(realpath "$0")"
SCRIPT_DIRECTORY="$(dirname "$FULL_PATH_TO_SCRIPT")"

RETURN_CODE_SUCCESS=0
RETURN_CODE_ERROR=1

# Find all files in SCRIPT_DIRECTORY with one of these extensions
FILE_LIST="$(find "$SCRIPT_DIRECTORY" | grep -E ".*\.(ino|cpp|c|h|hpp|hh)$")"
IFS=$'\n' read -r -d '' -a FILE_LIST_ARRAY <<< "$FILE_LIST"

num_files="${#FILE_LIST_ARRAY[@]}"
echo -e "$num_files files found to format:"
if [ "$num_files" -eq 0 ]; then
    echo "Nothing to do."
    exit $RETURN_CODE_SUCCESS
fi

#-------------------------------------------------------------------------------------------------
# Script for Clang-Format
#-------------------------------------------------------------------------------------------------
clang-format --verbose -i --style=file "${FILE_LIST_ARRAY[@]}"
